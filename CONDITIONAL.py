#!/usr/bin/env python
# coding: utf-8

# # IF-ELSE

# In[1]:


a = 33
b = 200
if b > a:
  print("b is greater than a")


# In[2]:


a = 33
b = 33
if b > a:
  print("b is greater than a")
elif a == b:
  print("a and b are equal")


# In[3]:


a = 200
b = 33
if b > a:
  print("b is greater than a")
elif a == b:
  print("a and b are equal")
else:
  print("a is greater than b")


# In[4]:


if a > b: print("a is greater than b")


# In[5]:


a = 2
b = 330
print("A") if a > b else print("B")


# In[6]:


a = 330
b = 330
print("A") if a > b else print("=") if a == b else print("B")


# In[7]:


a = 200
b = 33
c = 500
if a > b and c > a:
  print("Both conditions are True")


# # NESTED IF

# In[8]:


x = 41

if x > 10:
  print("Above ten,")
  if x > 20:
    print("and also above 20!")
  else:
    print("but not above 20.")


# # WHILE LOOP-With the while loop we can execute a set of statements as long as a condition is true

# In[3]:


i = 1
while i < 6:
  print(i)
  i += 1


# In[7]:


i = 1
while i < 6:
  print(i)
  if i == 3:
    break
  i += 1


# In[1]:


i = 0
while i < 6:
  i += 1
  if i == 3:
    continue
  print(i)


# In[12]:


i = 1
while i < 6:
  print(i)
  i += 1
else:
  print("i is no longer less than 6")

